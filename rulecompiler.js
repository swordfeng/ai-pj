
function ParseError(pos, message) {
    this.pos = pos;
    this.message = message;
    //console.log(`failure on ${pos} with message '${message}'`);
}

function orElse() {
    let list = arguments;
    return function (ctx) {
        for (let i = 0; i < list.length; i++) {
            let r = list[i](ctx);
            if (r !== null) return r;
        }
        return null;
    }
}

function removeComment(str) {
    let result = '';
    let comment = false;
    for (let i = 0; i < str.length; i++) {
        if (str[i] === '#') {
            comment = true;
        } else if (str[i] === '\n') {
            comment = false;
        }
        if (comment) result += ' ';
        else result += str[i];
    }
    return result;
    //return str.replace(/#.*(\n|$)/g, '');
}

function spaces(ctx) {
    while (ctx.pos < ctx.rules.length && ' \t\r\n'.includes(ctx.rules[ctx.pos])) {
        ctx.pos++;
    }
    return true;
}

function tryp(comb) {
    return function (ctx) {
        let temppos = ctx.pos;
        try {
            return comb(ctx);
        } catch (e) {
            if (!(e instanceof ParseError)) {
                throw e;
            }
            ctx.pos = temppos;
            return null;
        }
    }
}

function identifier(ctx) {
    spaces(ctx);
    const ident = /^[a-z0-9_]+$/i;
    let id = '';
    while (ctx.pos < ctx.rules.length) {
        if (ident.test(ctx.rules[ctx.pos])) {
            id += ctx.rules[ctx.pos++];
        } else break;
    }
    if (id === '') {
        if (ctx.pos === ctx.rules.length) {
            throw new ParseError(ctx.pos, 'unexpected end of file');
        } else {
            throw new ParseError(ctx.pos, `unexpected char '${ctx.rules[ctx.pos]}'`);
        }
    }
    return id;
}

function keyword(w) {
    return function (ctx) {
        spaces(ctx);
        let pos = ctx.pos;
        let id = identifier(ctx);
        if (id === w) return true;
        throw new ParseError(pos, `unexpected identifier '${id}', expected '${w}'`);
    }
}

function operator(op) {
    return function (ctx) {
        spaces(ctx);
        if (ctx.pos + op.length > ctx.rules.length) {
            throw new ParseError(ctx.rules.length, 'unexpected end of file');
        }
        if (ctx.rules.slice(ctx.pos, ctx.pos + op.length) === op) {
            ctx.pos += op.length;
            return true;
        }
        throw new ParseError(ctx.pos, `unexpected token, expected '${op}'`);
    }
}

function number(ctx) {
    let minus = false;
    if (!tryp(operator('+'))(ctx)) {
        minus = tryp(operator('-'))(ctx);
    }
    spaces(ctx);
    const numchar = /^([0-9]+|[0-9]*\.[0-9]+)$/;
    let num = '';
    while (ctx.pos < ctx.rules.length) {
        if (numchar.test(num + ctx.rules[ctx.pos])) {
            num += ctx.rules[ctx.pos++];
        } else break;
    }
    if (!numchar.test(num)) {
        throw new ParseError(ctx.pos, 'expected number');
    }
    return minus ? -parseFloat(num) : parseFloat(num);
}

function sepBy(sep, comb) {
    if (typeof sep === 'string') sep = operator(sep);
    return function (ctx) {
        let list = [];
        list.push(comb(ctx));
        while (tryp(ctx => { sep(ctx); list.push(comb(ctx)); return true })(ctx));
        return list;
    }
}

function exp(ctx) {
    return expOr(ctx);
}

function expOr(ctx) {
    let list = sepBy('OR', expAnd)(ctx);
    if (list.length === 0) throw new ParseError(ctx.pos, 'expected experssion');
    if (list.length === 1) return list[0];
    return {
        type: 'exp',
        op: 'OR',
        args: list
    };
}

function expAnd(ctx) {
    let list = sepBy('AND', expNot)(ctx);
    if (list.length === 0) throw new ParseError(ctx.pos, 'expected experssion');
    if (list.length === 1) return list[0];
    return {
        type: 'exp',
        op: 'AND',
        args: list
    };
}

function expNot(ctx) {
    if (tryp(operator('NOT'))(ctx)) {
        return {
            type: 'exp',
            op: 'NOT',
            arg: expBool(ctx)
        };
    } else {
        return expBool(ctx);
    }
}

function expBool(ctx) {
    return tryp(expEq)(ctx) || tryp(expNeq)(ctx) || expUnit(ctx);
}

function expEq(ctx) {
    let lhs = expUnit(ctx);
    operator('==')(ctx);
    let rhs = expUnit(ctx);
    return {
        type: 'exp',
        op: '==',
        lhs, rhs
    };
}

function expNeq(ctx) {
    let lhs = expUnit(ctx);
    operator('!=')(ctx);
    let rhs = expUnit(ctx);
    return {
        type: 'exp',
        op: '!=',
        lhs, rhs
    };
}

function expUnit(ctx) {
    if (tryp(operator('('))(ctx)) {
        let e = exp(ctx);
        operator(')')(ctx);
        return e;
    }
    return tryp(value)(ctx) || expFunc(ctx);
}

function value(ctx) {
    if (tryp(operator('TRUE'))(ctx)) {
        return {
            type: 'value',
            value: true
        };
    }
    if (tryp(operator('FALSE'))(ctx)) {
        return {
            type: 'value',
            value: false
        };
    }
    return {
        type: 'value',
        value: number(ctx)
    };
}

function expFunc(ctx) {
    let name = identifier(ctx);
    operator('(')(ctx);
    let args = sepBy(',', funcParam)(ctx);;
    operator(')')(ctx);
    return {
        type: 'func',
        name, args
    };
}

function funcParam(ctx) {
    let param = '';
    while (ctx.pos < ctx.rules.length) {
        let c = ctx.rules[ctx.pos];
        if (c === ',' || c === ')') break;
        param += c;
        ctx.pos++;
    }
    return param;
}

function rule(ctx) {
    spaces(ctx);
    if (ctx.rules_line_length === undefined) {
        ctx.rules_line_length = ctx.rules.split('\n').map(x => x.length);
    }
    let pos = ctx.pos;
    let line = 0;
    for (let i of ctx.rules_line_length) {
        pos -= i + 1;
        line++;
        if (pos < 0) break;
    }
    let posStart = ctx.pos;
    let prio = tryp(ctx => (keyword('PRIO')(ctx), number(ctx)))(ctx) || 0;
    let quant = (tryp(keyword('FORALL_PERSPECTIVE'))(ctx) && 'allperspective') ||
                (tryp(keyword('ANY_PERSPECTIVE'))(ctx) && 'anyperspective');
    keyword('IF')(ctx);
    let cond = exp(ctx);
    keyword('THEN')(ctx);
    let action = expFunc(ctx);
    operator(';')(ctx);
    let posEnd = ctx.pos;

    return {
        type: 'rule',
        code: ctx.orig_rules.slice(posStart, posEnd),
        line, prio, quant, cond, action
    };
}

function rules(ctx) {
    let result = sepBy(spaces, rule)(ctx);
    spaces(ctx);
    if (ctx.pos !== ctx.rules.length) {
        rule(ctx);
    }
    return result;
}

function createRules(r) {
    try {
        return rules({
            orig_rules: r,
            rules: removeComment(r),
            pos: 0
        });
    } catch (e) {
        if (e instanceof ParseError) {
            let rlist = r.split('\n');
            let line = 1;
            let col = e.pos;
            while (col >= rlist[line-1].length+1) {
                col -= rlist[line-1].length+1;
                line++;
            }
            col++;
            if (line > rlist.length) {
                line = rlist.length;
                col = rlist[line-1].length+1;
            }
            console.error(`At ${line}:${col}:`);
            console.error('      '+rlist[line-1]);
            let sp = '';
            for (let i = 0; i < col-1; i++) sp += ' ';
            console.error('      '+sp + '^');
            console.error(e.message);
            throw `At ${line}:${col}:\n` + '      '+rlist[line-1] + '\n      '+sp + '^\n' + e.message;
        } else throw e;
        return null;
    }
}

module.exports = createRules;
