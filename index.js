const {spawn} = require('child_process');
const fs = require('fs');
const util = require('util');
let Engine = require('./engine.js');
const {newCube, applyFormula, reverseFormula, copyCube} = require('./cubemodel.js');

const twistDuration = 200;

let container = document.querySelector('#container');
let cube = null;
let light = null;
let engine = null;
let playing = false;

let reset = document.querySelector('#reset');
let play = document.querySelector('#play');
let step = document.querySelector('#step');
let stop = document.querySelector('#stop');
let rules = document.querySelector('#rules');
let cub = document.querySelector('#cube');
let gen = document.querySelector('#gen');

function doReset() {
    let cubemodel = JSON.parse(fs.readFileSync('cube.txt').toString());
    let rules = fs.readFileSync('rules.txt').toString();
    let tmpeng = new Engine(rules, cubemodel);
    if (tmpeng.error) {
        logger(tmpeng.error);
        return;
    }
    while (!tmpeng.stopped) tmpeng.step();
    //logger(tmpeng.applyHistory);
    let seq = reverseFormula(tmpeng.applyHistory);

    if (cube) {
        container.removeChild(cube.domElement);
    }
    cube = new ERNO.Cube({twistDuration: 0});
    cube.twist(seq);
    let loaded = false;
    cube.addEventListener('onTwistComplete', () => {
        if (cube.twistQueue.future.length === 0) {
            if (!loaded) {
                loaded = true;
                container.appendChild(cube.domElement);
            }
            if (playing) {
                playStep();
            } else {
                play.disabled = false;
                step.disabled = false;
                stop.disabled = true;
            }
        }
    });
    stop.disabled = true;
    cubemodel = JSON.parse(fs.readFileSync('cube.txt').toString());
    engine = new Engine(rules, cubemodel);
    logger('engine loaded');
}

function logger(text) {
    let log = document.querySelector('#log');
    log.innerHTML += text + '\n';
    log.scrollTop = log.scrollHeight - log.clientHeight;

}

window.addEventListener('load', () => {
    light = new Photon.Light(10, 0, 100);
    doReset();
});

function playStep() {
    if (engine.stopped) {
        playing = false;
        play.disabled = false;
        step.disabled = false;
        stop.disabled = true;
        return;
    }
    let stepResult = engine.step();
    logger(stepResult.log);
    if (stepResult.applied) {
        play.disabled = true;
        step.disabled = true;
        stop.disabled = false;
        cube.twistDuration = twistDuration;
        cube.twist(stepResult.applied);
    } else if (playing) {
        playStep();
    } else {
        play.disabled = false;
        step.disabled = false;
        stop.disabled = true;
    }
}

function playGo() {
    playing = true;
    playStep();
}

reset.addEventListener('click', () => window.location.reload());
play.addEventListener('click', playGo);
step.addEventListener('click', playStep);
stop.addEventListener('click', () => {
    if (playing) {
        playing = false;
        stop.disabled = true;
    }
})
rules.addEventListener('click', () => {
    spawn('xdg-open', ['rules.txt']);
});
cub.addEventListener('click', () => {
    spawn('xdg-open', ['cube.txt']);
});
gen.addEventListener('click', () => {
    const chars = 'LlRrUuDdFfBb';
    let seq = ''
    for (let i = 0; i < 50; i++) {
        seq += chars[parseInt(Math.random()*chars.length)];
    }
    logger(`generated seq: ${seq}`)
    let cube = newCube();
    applyFormula(cube, seq);
    logger('generated cube:');
    logger(util.inspect(cube, {depth: 3}));
    fs.writeFileSync('cube.txt', util.inspect(cube, {depth: 3}));
});
