
function newCube() {
    return [
        [ [0, 0, 0]
        , [0, 0, 0]
        , [0, 0, 0]],
        [ [1, 1, 1]
        , [1, 1, 1]
        , [1, 1, 1]],
        [ [2, 2, 2]
        , [2, 2, 2]
        , [2, 2, 2]],
        [ [3, 3, 3]
        , [3, 3, 3]
        , [3, 3, 3]],
        [ [4, 4, 4]
        , [4, 4, 4]
        , [4, 4, 4]],
        [ [5, 5, 5]
        , [5, 5, 5]
        , [5, 5, 5]]
    ];
}

const origCube = newCube();

function rotate(cube, axis, layer, count) {
    function rotate_seq(a, b, c, d) {
        let tmp = cube[d][layer].slice();
        cube[d][layer] = [cube[c][2][layer], cube[c][1][layer], cube[c][0][layer]];
        [cube[c][2][layer], cube[c][1][layer], cube[c][0][layer]] = [cube[b][layer][2], cube[b][layer][1], cube[b][layer][0]];
        [cube[b][layer][2], cube[b][layer][1], cube[b][layer][0]] = [cube[a][0][layer], cube[a][1][layer], cube[a][2][layer]];
        [cube[a][0][layer], cube[a][1][layer], cube[a][2][layer]] = tmp;
    }
    function rotate_face(face, times) {
        const c = cube[face];
        while (times-- > 0) {
            let tmp = c[2][0];
            c[2][0] = c[0][0];
            c[0][0] = c[0][2];
            c[0][2] = c[2][2];
            c[2][2] = tmp;
            tmp = c[0][1];
            c[0][1] = c[1][2];
            c[1][2] = c[2][1];
            c[2][1] = c[1][0];
            c[1][0] = tmp;
        }
    }
    while (count-- > 0) {
        if (axis === 'x') {
            rotate_seq(0, 1, 5, 4);
            if (layer === 0) {
                rotate_face(2, 1);
            } else if (layer === 2) {
                rotate_face(3, 1);
            }
        } else if (axis === 'y') {
            rotate_seq(1, 2, 4, 3);
            if (layer === 0) {
                rotate_face(0, 1);
            } else if (layer === 2) {
                rotate_face(5, 1);
            }
        } else if (axis === 'z') {
            rotate_seq(2, 0, 3, 5);
            if (layer === 0) {
                rotate_face(1, 1);
            } else if (layer === 2) {
                rotate_face(4, 1);
            }
        }
    }
}

function colors2pos() {
    let colors = Array.prototype.slice.apply(arguments);
    let x = 1, y = 1, z = 1;
    const acts = [
        () => y--,
        () => z--,
        () => x--,
        () => x++,
        () => z++,
        () => y++
    ];
    for (let c of colors) acts[c]();
    return [x, y, z];
}

function colors2block() {
    let [x, y, z] = colors2pos.apply(null, arguments);
    return pos2block(x, y, z);
}

function pos2block(x, y, z) {
    return x + y*3 + z*9;
}

function getBlockColors(cube, x, y, z) {
    let colors = [];
    if (x === 0) colors.push(cube[2][y][z]);
    if (x === 2) colors.push(cube[3][y][z]);
    if (y === 0) colors.push(cube[0][z][x]);
    if (y === 2) colors.push(cube[5][z][x]);
    if (z === 0) colors.push(cube[1][x][y]);
    if (z === 2) colors.push(cube[4][x][y]);
    return colors;
}

function getBlockColor(cube, x, y, z, f) {
    if ((f === 0 && y === 0) || (f === 5 && y === 2)) return cube[f][z][x];
    if ((f === 1 && z === 0) || (f === 4 && z === 2)) return cube[f][x][y];
    if ((f === 2 && x === 0) || (f === 3 && x === 2)) return cube[f][y][z];
    throw new Error('not outer surface');
}

function applyFormula(cube, seq) {
    for (let i = 0; i < seq.length; i++) {
        ({
            'U': () => rotate(cube, 'y', 2, 1),
            'u': () => rotate(cube, 'y', 2, 3),
            'D': () => rotate(cube, 'y', 0, 3),
            'd': () => rotate(cube, 'y', 0, 1),
            'B': () => rotate(cube, 'z', 2, 1),
            'b': () => rotate(cube, 'z', 2, 3),
            'F': () => rotate(cube, 'z', 0, 3),
            'f': () => rotate(cube, 'z', 0, 1),
            'R': () => rotate(cube, 'x', 2, 1),
            'r': () => rotate(cube, 'x', 2, 3),
            'L': () => rotate(cube, 'x', 0, 3),
            'l': () => rotate(cube, 'x', 0, 1),
        }[seq[i]])();
    }
}

function reverseFormula(seq) {
    let result = '';
    for (let i = seq.length - 1; i >= 0; i--) {
        let c = seq[i];
        if (c === c.toLowerCase()) result += c.toUpperCase();
        else result += c.toLowerCase();
    }
    return result;
}

function copyCube(cube) {
    let result = newCube();
    for (let i = 0; i < 3; i++)
        for (let j = 0; i < 3; i++)
            for (let k = 0; i < 3; i++)
                result[i][j][k] = cube[i][j][k];
    return result;
}

module.exports = {
    colors2block,
    pos2block,
    getBlockColors,
    getBlockColor,
    applyFormula,
    newCube,
    reverseFormula,
    copyCube
};
/*
let cube = newCube();
rotate(cube, 'x', 0, 1);
rotate(cube, 'y', 0, 2);
rotate(cube, 'z', 0, 1);
console.log(cube);
*/
