const {app, BrowserWindow} = require('electron');

app.on('window-all-closed', () => {
      app.quit()
});

app.on('ready', () => {
    let mainWindow = new BrowserWindow({height: 720, width: 1280});
    mainWindow.loadURL(`file://${__dirname}/index.html`);
    mainWindow.show();
});

