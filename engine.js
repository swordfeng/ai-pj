const fs = require('fs');
const compile = require('./rulecompiler');
const {
    colors2block,
    pos2block,
    getBlockColors,
    getBlockColor,
    applyFormula,
    newCube
} = require('./cubemodel');

function Engine(rules, cube) {
    // ruleset, model, state, eval functions, actions
    try {
        this.ruleset = compile(rules);
    } catch (e) {
        this.stopped = true;
        if (typeof e === 'string') logger(e);
        else throw e;
    }
    if (this.ruleset === null) return;
    this.cube = cube;
    this.state = {};

    this.evalFunc = {};
    this.actFunc = {};

    this.applyHistory = '';
    this.stopped = false;
    // ruleset sorted by prio
    this.ruleset = this.ruleset.sort((l, r) => r.prio - l.prio);

    this.applied = '';

    const addEvalFunc = (name, func) => {
        this.evalFunc[name] = func;
    }
    const addActFunc = (name, func) => {
        this.actFunc[name] = func;
    }

    addEvalFunc('getblock', (args, perspective) => {
        let x = parseInt(args[0]);
        let y = parseInt(args[1]);
        let z = parseInt(args[2]);
        [x, y, z] = coordTransform(perspective, x, y, z);
        let colors = getBlockColors(this.cube, x, y, z);
        return colors2block.apply(null, colors);
    });
    addEvalFunc('BLOCK', (args, perspective) => {
        let x = parseInt(args[0]);
        let y = parseInt(args[1]);
        let z = parseInt(args[2]);
        [x, y, z] = coordTransform(perspective, x, y, z);
        return pos2block(x, y, z);
    });
    addEvalFunc('checkblock', (args, perspective) => {
        if (this.evalFunc.getblock(args, perspective) !== this.evalFunc.BLOCK(args, perspective)) return false;
        let x = parseInt(args[0]);
        let y = parseInt(args[1]);
        let z = parseInt(args[2]);
        [x, y, z] = coordTransform(perspective, x, y, z);
        if (x === 0) return this.evalFunc.color([x, y, z, 2], 1) === 2;
        if (x === 2) return this.evalFunc.color([x, y, z, 3], 1) === 3;
        if (y === 0) return this.evalFunc.color([x, y, z, 0], 1) === 0;
        if (y === 2) return this.evalFunc.color([x, y, z, 5], 1) === 5;
        if (z === 0) return this.evalFunc.color([x, y, z, 1], 1) === 1;
        if (z === 2) return this.evalFunc.color([x, y, z, 4], 1) === 4;
        throw new Error('wtf?!');
    });
    addEvalFunc('color', (args, perspective) => {
        let x = parseInt(args[0]);
        let y = parseInt(args[1]);
        let z = parseInt(args[2]);
        let surface = parseInt(args[3]);
        [x, y, z] = coordTransform(perspective, x, y, z);
        surface = surfaceTransform(perspective, surface);
        return getBlockColor(this.cube, x, y, z, surface);
    });
    addEvalFunc('state', (args, perspective) => {
        if (this.state[args[0]]) return true;
        return false;
    });

    addActFunc('SETSTATE', (args, perspective) => {
        this.state[args[0]] = true;
    });
    addActFunc('STOP', (args, perspective) => {
        this.stopped = true;
    });
    addActFunc('APPLY', (args, perspective) => {
        let seq = formulaTransform(perspective, args[0]);
        //console.log('apply:', seq);
        //console.log();
        this.ap(seq);
    });
}

Engine.prototype.step = function () {
    if (this.stopped) return null;
    let ret = {
        log: ''
    };
    let activerules = []
    for (let i = 0; i < this.ruleset.length; i++) {
        let rule = this.ruleset[i];
        //console.log(`check RULE L${rule.line}`)
        let active;
        let perspective = 1;
        if (rule.quant === 'allperspective') {
            active = true;
            for (let p = 1; p <= 4; p++) {
                if (!this.evaluate(rule.cond, p)) {
                    active = false;
                    break;
                }
            }
        } else if (rule.quant === 'anyperspective') {
            active = false;
            for (let p = 1; p <= 4; p++) {
                if (this.evaluate(rule.cond, p)) {
                    active = true;
                    perspective = p;
                    break;
                }
            }
        } else { // rule.quant === null
            active = this.evaluate(rule.cond, 1);
        }
        if (active) {
            activerules.push([i, perspective]);
            // as rules are sorted in priority,
            // no need to find other active rules
            break;
        }
    }
    for (let [i, perspective] of activerules) {
        ret.log += `activate RULE L${this.ruleset[i].line} on perspective ${perspective}:\n`;
        ret.log +=  this.ruleset[i].code + '\n';
        this.act(this.ruleset[i].action, perspective);
        this.applyHistory += this.applied;
        ret.applied = this.applied;
        this.applied = '';
    }
    return ret;
}

Engine.prototype.evaluate = function (cond, perspective) {
    //let result = (() => {
    // return boolean
    if (cond.type === 'value') {
        return cond.value;
    } else if (cond.type === 'func') {
        if (this.evalFunc[cond.name] === undefined) throw new Error(`function '${cond.name}' not found`);
        return this.evalFunc[cond.name](cond.args, perspective);
    } else if (cond.type === 'exp') {
        if (cond.op === 'AND') {
            for (let subcond of cond.args) {
                if (!this.evaluate(subcond, perspective)) return false;
            }
            return true;
        } else if (cond.op === 'OR') {
            for (let subcond of cond.args) {
                if (this.evaluate(subcond, perspective)) return true;
            }
            return false;
        } else if (cond.op === 'NOT') {
            return !this.evaluate(cond.arg, perspective);
        } else if (cond.op === '==') {
            return this.evaluate(cond.lhs, perspective) === this.evaluate(cond.rhs, perspective);
        } else if (cond.op === '!=') {
            return this.evaluate(cond.lhs, perspective) !== this.evaluate(cond.rhs, perspective);
        }
    } else throw new Error('unsupported cond type')
}

Engine.prototype.act = function (func, perspective) {
    if (func.type !== 'func') throw new Error('unsupported action');
    if (this.actFunc[func.name] === undefined) throw new Error(`function '${func.name}' not found`);
    this.actFunc[func.name](func.args, perspective);
}

Engine.prototype.ap = function (seq) {
    this.applied += seq;
    applyFormula(this.cube, seq);
}

function coordTransform(perspective, x, y, z) {
    if (perspective === 1) return [x, y, z];
    else if (perspective === 2) return [z, y, 2-x];
    else if (perspective === 3) return [2-z, y, x];
    else if (perspective === 4) return [2-x, y, 2-z];
    else throw new Error(`unknown perspective ${perspective}`);
}

function surfaceTransform(perspective, f) {
    if (f === 0 || f === 5) return f;
    else if (perspective === 1) return f;
    else if (perspective === 2) return [2, 4, 1, 3][f-1];
    else if (perspective === 3) return [3, 1, 4, 2][f-1];
    else if (perspective === 4) return [4, 3, 2, 1][f-1];
    else throw new Error(`unknown perspective ${perspective}`);
}

function formulaTransform(perspective, seq) {
    let result = '';
    function getTransform(char, times) {
        const transform = {
            'f': 'l',
            'l': 'b',
            'b': 'r',
            'r': 'f'
        };
        let c = char.toLowerCase();
        let upper = (c !== char);
        while (times-- > 0) {
            c = transform[c];
        }
        if (upper) c = c.toUpperCase();
        return c;
    }
    for (let i = 0; i < seq.length; i++) {
        let c = seq[i];
        if ('UuDd'.includes(c)) result += c;
        else if (perspective === 1) result += c;
        else if (perspective === 2) result += getTransform(c, 1);
        else if (perspective === 4) result += getTransform(c, 2);
        else if (perspective === 3) result += getTransform(c, 3);
        else throw new Error(`unknown perspective ${perspective}`);
    }
    return result;
}

/*
let cube = [ [ [ 2, 2, 1 ], [ 0, 0, 0 ], [ 4, 4, 4 ] ],
  [ [ 0, 1, 1 ], [ 0, 1, 1 ], [ 3, 3, 3 ] ],
  [ [ 1, 1, 0 ], [ 5, 2, 2 ], [ 5, 2, 2 ] ],
  [ [ 0, 4, 5 ], [ 0, 3, 3 ], [ 0, 3, 3 ] ],
  [ [ 2, 5, 5 ], [ 2, 4, 4 ], [ 2, 4, 4 ] ],
  [ [ 3, 3, 4 ], [ 1, 5, 5 ], [ 1, 5, 5 ] ] ];
*/

/*
let cube = newCube();
applyFormula(cube, 'ldfUUFRFRRURFUBlbUBBUFrfFUfLflFufUrURLulUURurlULUfUF');

let engine = new Engine(fs.readFileSync('rules.txt').toString(), cube);

for (let i = 0; i < 50; i++) {
    let result = engine.step();
    if (result === null) break;
    console.log(result.log);
}
console.log(engine.applyHistory);
*/

module.exports = Engine;
